package com.trestian.sda.gym;

import com.trestian.sda.gym.service.TrainerService;

public class MainApp {

    public static void main(String[] args) {
        TrainerService trainerService = new TrainerService();

        System.out.println(trainerService.getAllTrainers());
    }
}
