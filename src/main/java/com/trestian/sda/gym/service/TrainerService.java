package com.trestian.sda.gym.service;

import com.trestian.sda.gym.dao.TrainerDao;
import com.trestian.sda.gym.model.Trainer;

import java.util.List;

public class TrainerService {

    private TrainerDao trainerDao = new TrainerDao();

    // CRUD Operations + business logic

    public List<Trainer> getAllTrainers(){
        List<Trainer> allTrainers = trainerDao.getAllTrainers();
        return allTrainers;
    }

    public Trainer getOldestTrainer(){
        List<Trainer> allTrainers = trainerDao.getAllTrainers();
        Trainer oldestTrainer = allTrainers.get(0);
        for (Trainer trainer : allTrainers) {
//            if (trainer.getAge() > oldestTrainer.getAge()){
//                oldestTrainer = trainer;
//            }
        }

        return oldestTrainer;
    }
}
