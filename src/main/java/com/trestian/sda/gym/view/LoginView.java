package com.trestian.sda.gym.view;

import com.trestian.sda.gym.model.Trainer;
import com.trestian.sda.gym.service.TrainerService;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LoginView extends Application {

    private TrainerService trainerService = new TrainerService();
//    private UserService userService = new UserService();

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("GYM APP");

        VBox rootVBox = new VBox();

        //Add elements here:

        Text trainersText = new Text();
        List<Trainer> allTrainers = trainerService.getAllTrainers();

        String trainersString = allTrainers.toString();
        trainersText.setText(trainersString);


        //Trainers List :
        ListView<String> trainersListView = new ListView<>();

        List<String> trainersNames = new ArrayList<>();
        for (Trainer trainer: allTrainers){
            trainersNames.add(trainer.getLastName());
        }
//        List<String> trainersNames = allTrainers.stream().map(trainer ->trainer.getLastName()).collect(Collectors.toList());

        trainersListView.getItems().addAll(trainersNames);
        //End add elements

        // Table Trainers

        TableView<Trainer> trainersTableView = new TableView<>();
        ObservableList<Trainer> trainers = FXCollections.observableArrayList();
        trainers.addAll(allTrainers);
        trainersTableView.setItems(trainers);

        TableColumn firstNameColumn = new TableColumn("First Name");
        firstNameColumn.setCellValueFactory(
                new PropertyValueFactory<Trainer, String>("firstName"));

        TableColumn lastNameColumn = new TableColumn("Last Name");
        lastNameColumn.setCellValueFactory(
                new PropertyValueFactory<Trainer, String>("lastName"));

        TableColumn expColumn = new TableColumn("Years of Experience");
        expColumn.setCellValueFactory(
                new PropertyValueFactory<Trainer, Integer>("experience"));

        trainersTableView.getColumns().addAll(firstNameColumn,lastNameColumn,expColumn);

        rootVBox.getChildren().addAll(trainersText,trainersListView,trainersTableView);
        Scene scene = new Scene(rootVBox);

        primaryStage.setScene(scene);
        primaryStage.show();

//        userService.getUserByUsernameAndPassword(username,password);
    }

    //build javaFX login view

}
