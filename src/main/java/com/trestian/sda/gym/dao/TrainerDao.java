package com.trestian.sda.gym.dao;

import com.trestian.sda.gym.model.Trainer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class TrainerDao {

    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
    // SessionFactory

    //CRUD operations, methods use Session and Transaction
    // Query the DB using SQL, HQL or Criteria

    //getAllTrainers()

    public List<Trainer> getAllTrainers(){
        //TODO: use Session and Transaction
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        // Query the DB using SQL, HQL or Criteria
        Query<Trainer> query = session.createQuery("from Trainer");
        List<Trainer> trainers = query.list();
        tx.commit();
        session.close();

        return trainers;
    }

    //getTrainerById(int trainerId)

    //createTrainer(Trainer trainer);

    //deleteTrainer(Trainer trainer);
    //deleteTrainer(int trainerId);

}
